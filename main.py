import numpy as np
from jacobMethod import jacobo

# =========================== #
#   Grzegorz Kucharski 229932 #
#   Wojciech Cwynar    229856 #
#   Grupa 5                   #
# =========================== #


while True:
    print("Program wykorzystujący metode iteracyjna Jacobiego do rozwiązywania x rownan liniowych z x niewiadomymi ")
    correct = False
    data = None
    while correct is not True:
        # file_name = "matrix"
        try:
            print("Wpisz nazwa pliku zawierającego macierz: ")
            file_name = str(input())
            data = np.genfromtxt("Matrixes/" + file_name, delimiter=",")
            print(data)
            correct = True
        except ValueError:
            print("Podana macierz jest nieprawidłowa")
            correct = False
        except OSError:
            print("Podana macierz jest nieprawidłowa")
            correct = False

    rows = len(data)

    correct = False
    stop_condition = 0
    while correct is not True:
        print("""\nWybierz kryterium zatrzymania
        1 - Dokładność
        2 - iteracja""")
        try:
            stop_condition = int(input())
            if 0 < stop_condition < 3:
                correct = True
            else:
                print("Brak takiego kryterium")
                correct = False
        except ValueError:
            print("Brak takiego kryterium")
            correct = False

    correct = False
    accuracy = 0
    iteration = 0
    while correct is not True:
        if stop_condition == 1:
            try:
                accuracy = abs(float(input("Podaj dokładność epsilon: ")))
                iteration = -1
                correct = True
            except ValueError:
                print("Błedna liczba")
                correct = False
        else:
            try:
                iteration = int(input("Podaj liczbe iteracji: "))
                accuracy = -1
                correct = True
            except ValueError:
                print("Błedna liczba")
                correct = False
    results = jacobo(rows, accuracy, iteration, data)
    if results != -1:
        for i in range(rows):
            print("X" + str(i + 1) + " = " + str(results[i]).rstrip("0"))
        print("\n")
