import numpy as np
import sympy as sp


# Funckja oblicza pierwiastki układu równań przekazanego w parametrze matrix
def jacobo(ile_rownan, eps, iteration, matrix):
    counter = 1  # licznik iteracji algorytmu w przypadku szukania zadanej dokładności
    # sprawdzenie czy wyznacznik macierzu powstałej z układu != sie zero
    for i in range(ile_rownan):
        if matrix[i, i] == 0:
            print("Wartości na przekątnej nie mogą rówać się 0\n")
            return -1

    # sprawdzenie czy wiersze*Diag^-1 są mniejsze niz 1 (zbiezność)
    for i in range(ile_rownan):
        suma = 0
        diag = matrix[i, i]
        for j in range(ile_rownan):
            if i != j:
                suma += abs(matrix[i, j])
        if suma >= diag:
            print("Macierz nie spełnia założeń zbieżności\n")
            return -1

    # Macierz współczynników
    A = sp.Matrix(matrix)

    # Macierz wyrazow wolnych
    b = A[:, ile_rownan]

    A.col_del(ile_rownan)

    # Macierz niewiadomych (same zera)
    x = sp.zeros(ile_rownan, 1)

    D = sp.zeros(ile_rownan, ile_rownan)
    LU = sp.zeros(ile_rownan, ile_rownan)

    # Tworzenie macierzy D i LU
    for i in range(ile_rownan):
        for j in range(ile_rownan):
            if i == j:
                D[i, j] = A[i, j]
            else:
                LU[i, j] = A[i, j]

    # D do potegi -1
    N = D ** -1

    # Obliczanie macierzy M jako iloczynu -N i LU
    M = -N * LU

    if iteration != -1:  # Jezeli uzytkownik wybral iteracje jako kryterium stopu
        for n in range(iteration):
            x = M * x + N * b
        return x
    else:  # Jezeli uzytkownik wybral dokladnosc jako kryterium stopu
        xm1 = None
        while True:
            isLessThanEpsilon = True
            x = M * x + N * b
            if xm1 is not None:  # Pomijane przy pierwszej iteracji
                for k in range(ile_rownan):
                    if x[k] - xm1[k] >= eps:  # kryterium stopu
                        isLessThanEpsilon = False
                        break
                counter += 1
                if isLessThanEpsilon:
                    print("Iteracji: " + str(counter))
                    return x
            xm1 = x
